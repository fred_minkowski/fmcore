#ifndef FMCORE_H
#define FMCORE_H
#include <iostream>
#include <math.h>

#define pi 3.14159265359

template<class T>
class Matrix{
	public:
		size_t Num_Rows;
		size_t Num_Cols;
		size_t Current_Row = 0;
		size_t Current_Col = 0;
		T* MatrixValues;
		Matrix<T>::Matrix(size_t NumCols);
		Matrix(size_t NumRows, size_t NumCols);
		Matrix(size_t NumRows, size_t NumCols, T* ArrayVal);
		~Matrix();
		void CompareDimensions(Matrix<T>& Mat1, Matrix<T>& Mat2);
		void CompareDimensionsMultiplication(Matrix<T>& Mat1, Matrix<T>& Mat2);
		void Matrix<T>::ValidIndex(Matrix<T>& Mat1, const size_t& Row, const size_t& Col);
		Matrix<T> operator+(const Matrix<T>& Val);
		void Matrix<T>::operator+=(const Matrix<T>& Val);
		template <class T>
		friend void operator+=(Matrix<T>& Mat, double Val);
		Matrix<T> Matrix<T>::operator()(const size_t& Row, const size_t& Col);
		Matrix<T> Matrix<T>::operator=(const T& Val);
		Matrix<T> Matrix<T>::operator=(const Matrix<T>& Val);
		Matrix<T> Matrix<T>::operator-(const Matrix<T>& Val);
		template <typename T>
		friend Matrix<T> operator*(const Matrix<T>& Mat, double Val);
		template <typename T>
		friend Matrix<T> operator*(double Val, const Matrix<T>& Mat);
		Matrix<T> Matrix<T>::operator*(Matrix<T>& Val);
		template <class T>
		friend Matrix<T> operator^(const Matrix<T>& Val, T Exp);
		Matrix<T> Matrix<T>::operator^(const Matrix<T>& Val);
		template <typename T>
		friend std::ostream& operator<<(std::ostream& os, const Matrix<T>& Val);
		void Eye(size_t dim);
		void Ones(size_t dim);
		void Zeros(size_t dim);
		T Get();
		T Get(const size_t& Row, const size_t& Col);
		T Set(const size_t& Row, const size_t& Col, T Val);
		Matrix<T> Transpose();
		static Matrix<T> Linspace(const T& Start, const T& End, const size_t& NumPoints);
		static Matrix<T> Range(const int& Start, const int& End, const size_t& Step);
};

template <class T>
Matrix<T>::Matrix(size_t NumCols)
{
	Num_Rows = 1;
	Num_Cols = NumCols;
	MatrixValues = new T[NumCols];
}

template <class T>
Matrix<T>::Matrix(size_t NumRows, size_t NumCols)
{
	Num_Rows = NumRows;
	Num_Cols = NumCols;
	MatrixValues = new T[NumRows*NumCols];
}

template <class T>
Matrix<T>::Matrix(size_t NumRows, size_t NumCols, T* ArrayVal)
{
	Num_Rows = NumRows;
	Num_Cols = NumCols;
	MatrixValues = new T[Num_Rows*Num_Cols];
	MatrixValues = ArrayVal;
}

template <class T>
Matrix<T>::~Matrix()
{
	// delete [] MatrixValues;
}

template <class T>
void Matrix<T>::CompareDimensions(Matrix<T>& Mat1, Matrix<T>& Mat2)
{
	try
	{
		if(!((Mat1.Num_Rows == Mat2.Num_Rows) && (Mat1.Num_Cols == Mat2.Num_Cols)) ? true : false)
			throw 1;
	}
	catch(int e)
	{
		std::cout<<"The matrix dimensions do not agree. \n";
		system("pause");
		exit(e);
	}
}

template <class T>
void Matrix<T>::CompareDimensionsMultiplication(Matrix<T>& Mat1, Matrix<T>& Mat2)
{
	try
	{
		if(!((Mat1.Num_Rows == Mat2.Num_Cols) && (Mat1.Num_Cols == Mat2.Num_Rows)) ? true : false)
			throw 1;
	}
	catch(int e)
	{
		std::cout<<"The matrix dimensions do not agree. \n";
		system("pause");
		exit(e);
	}
}

template <class T>
void Matrix<T>::ValidIndex(Matrix<T>& Mat, const size_t& Row, const size_t& Col)
{
	try
	{
		if(!((Row < Mat.Num_Rows) && (Col < Mat.Num_Cols)) ? true : false)
			throw 1;
	}
	catch(int e)
	{
		std::cout<<"Attempting to access index outside of matrix.\n";
		system("pause");
		exit(e);
	}
}

template <class T>
Matrix<T> Matrix<T>::operator()(const size_t& Row, const size_t& Col)
{
	this->Current_Row = Row;
	this->Current_Col = Col;
	return *this;
}

template <class T>
Matrix<T> Matrix<T>::operator=(const T& Mat)
{
	this->MatrixValues[Current_Row*Num_Cols + Current_Col] = Mat;
	return *this;
}

template <class T>
Matrix<T> Matrix<T>::operator=(const Matrix<T>& Mat)
{
	T* TempVec = new T[Mat.Num_Rows*Mat.Num_Cols];
	for(size_t R = 0; R < Mat.Num_Rows; R++)
	{
		for(size_t C = 0; C < Mat.Num_Cols; C++)
		{
			MatrixValues[R*Mat.Num_Cols + C] = Mat.MatrixValues[R*Mat.Num_Cols + C];
		}
	}
	return *this;
}

template <class T>
Matrix<T> Matrix<T>::operator+(const Matrix<T>& Mat)
{
	Matrix<T>::CompareDimensions(*this, Mat);
	T* TempVec = new T[Num_Rows*Num_Cols];
	for(size_t R = 0; R < Num_Rows; R++)
	{
		for(size_t C = 0; C < Num_Cols; C++)
		{
			TempVec[R*Num_Cols + C] = 
				MatrixValues[R*Num_Cols + C] + Mat.MatrixValues[R*Num_Cols + C];
		}
	}
	return Matrix(Num_Rows, Num_Cols, TempVec);
}

template <class T>
void Matrix<T>::operator+=(const Matrix<T>& Mat)
{
	Matrix<T>::CompareDimensions(*this, Mat);
	for(size_t R = 0; R < Num_Rows; R++)
	{
		for(size_t C = 0; C < Num_Cols; C++)
		{
			MatrixValues[R*Num_Cols + C] += Mat.MatrixValues[R*Num_Cols + C];
		}
	}
}

template <class T>
void operator+=(Matrix<T>& Mat, double Val)
{
	for(size_t R = 0; R < Mat.Num_Rows; R++)
	{
		for(size_t C = 0; C < Mat.Num_Cols; C++)
		{
			Mat.MatrixValues[R*Mat.Num_Cols + C] += Val;
		}
	}
}

template <class T>
Matrix<T> Matrix<T>::operator-(const Matrix<T>& Val)
{
	Matrix<T>::CompareDimensions(*this, Val);
	T* TempVec = new T[Num_Rows*Num_Cols];
	for(size_t R = 0; R < Num_Rows; R++)
	{
		for(size_t C = 0; C < Num_Cols; C++)
		{
			TempVec[R*Num_Cols + C] = 
				MatrixValues[R*Num_Cols + C] - Val.MatrixValues[R*Num_Cols + C];
		}
	}
	return Matrix(Num_Rows, Num_Cols, TempVec);
}


//matrix multiplication
template <class T>
Matrix<T> Matrix<T>::operator*(Matrix<T>& Val)
{
	T* TempVec;
	if(Num_Rows == Num_Cols == 1)
	{
		TempVec = new T[Val.Num_Rows*Val.Num_Cols];
		T temp = MatrixValues[0];
		for(size_t R = 0; R < Val.Num_Rows; R++)
		{
			for(size_t C = 0; C < Val.Num_Cols; C++)
			{
				TempVec[R*Val.Num_Cols + C] = Val.MatrixValues[R*Val.Num_Cols + C] * temp;
			}
		}
		return Matrix(Val.Num_Rows, Val.Num_Cols, TempVec);
	}
	else if(Val.Num_Rows == Val.Num_Cols == 1)
	{
		TempVec = new T[Num_Rows*Num_Cols];
		T temp = Val.Get();
		for(size_t R = 0; R < Num_Rows; R++)
		{
			for(size_t C = 0; C < Num_Cols; C++)
			{
				TempVec[R*Num_Cols + C] = MatrixValues[R*Num_Cols + C] * temp;
			}
		}
		return Matrix(Num_Rows, Num_Cols, TempVec);
	}
	else
	{
		TempVec = new T[Num_Rows*Val.Num_Cols];
		Matrix<T>::CompareDimensionsMultiplication(*this, Val);
		for(size_t R = 0; R < Num_Rows; R++)
		{
			for(size_t C = 0; C < Val.Num_Cols; C++)
			{
				TempVec[R*Val.Num_Cols + C] = 0;
				for(size_t k = 0; k < Num_Cols; k++)
				{
					TempVec[R*Val.Num_Cols + C] += MatrixValues[R*Num_Cols + k] *
						Val.MatrixValues[k*Val.Num_Cols + C];
				}
			}
		}
		return Matrix(Num_Rows, Val.Num_Cols, TempVec);
	}
}

//matrix times number
template <class T>
Matrix<T> operator*(const Matrix<T>& Mat, double Val)
{
	T* TempVec = new T[Mat.Num_Rows*Mat.Num_Cols];
	for(size_t R = 0; R < Mat.Num_Rows; R++)
	{
		for(size_t C = 0; C < Mat.Num_Cols; C++)
		{
			TempVec[R*Mat.Num_Cols + C] = Mat.MatrixValues[R*Mat.Num_Cols + C]*Val;
		}
	}
	return Matrix<T>(Mat.Num_Rows, Mat.Num_Cols, TempVec);
}

//number times matrix
template <class T>
Matrix<T> operator*(double Val, const Matrix<T>& Mat)
{
	T* TempVec = new T[Mat.Num_Rows*Mat.Num_Cols];
	for(size_t R = 0; R < Mat.Num_Rows; R++)
	{
		for(size_t C = 0; C < Mat.Num_Cols; C++)
		{
			TempVec[R*Mat.Num_Cols + C] = Mat.MatrixValues[R*Mat.Num_Cols + C]*Val;
		}
	}
	return Matrix<T>(Mat.Num_Rows, Mat.Num_Cols, TempVec);
}

//element wise exponent product
template <class T>
Matrix<T> Matrix<T>::operator^(const Matrix<T>& Val)
{
	T* TempVec = new T[Num_Rows*Num_Cols];
	for(size_t R = 0; R < Num_Rows; R++)
	{
		for(size_t C = 0; C < Val.Num_Cols; C++)
		{
			TempVec[R*Num_Cols + C] = 
			pow(MatrixValues[R*Num_Cols + C], Val.MatrixValues[R*Num_Cols + C]);
			cout<<TempVec[R*Num_Cols + C]<<'\n';
		}
	}
	return Matrix(Num_Rows, Num_Cols, TempVec);
}

//element wise exponent product
template <class T>
Matrix<T> operator^(const Matrix<T>& Val, T Exp)
{
	T* TempVec = new T[Val.Num_Rows*Val.Num_Cols];
	for(size_t R = 0; R < Val.Num_Rows; R++)
	{
		for(size_t C = 0; C < Val.Num_Cols; C++)
		{
			Val.MatrixValues[R*Val.Num_Cols + C] = 
			pow(Val.MatrixValues[R*Val.Num_Cols + C], Exp);
		}
	}
	return Val;
}

template <class T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& Val)
{
	for(size_t R = 0; R < Val.Num_Rows; R++)
	{
		for(size_t C = 0; C < Val.Num_Cols; C++)
		{
			os << Val.MatrixValues[R*Val.Num_Cols + C] <<' ';
		}
		os << '\n';
	}
	os << '\n';
	return os;
}

template <class T>
void Matrix<T>::Eye(size_t dim)
{
	for(size_t R = 0; R < dim; R++)
	{
		for(size_t C = 0; C < dim; C++)
		{
			if(R == C)
				MatrixValues[R*dim + C] = 1;
			else
				MatrixValues[R*dim + C] = 0;
		}
	}
}

template <class T>
void Matrix<T>::Zeros(size_t dim)
{
	for(size_t R = 0; R < dim; R++)
	{
		for(size_t C = 0; C < dim; C++)
		{
			MatrixValues[R*dim + C] = 0;
		}
	}
}

template <class T>
void Matrix<T>::Ones(size_t dim)
{
	for(size_t R = 0; R < dim; R++)
	{
		for(size_t C = 0; C < dim; C++)
		{
			MatrixValues[R*dim + C] = 1;
		}
	}
}

template<class T>
T Matrix<T>::Get()
{
	return MatrixValues[0];
}


template<class T>
T Matrix<T>::Get(const size_t& Row, const size_t& Col)
{
	return MatrixValues[Row * Num_Cols + Col];
}

template<class T>
T Matrix<T>::Set(const size_t& Row, const size_t& Col, T Val)
{
	this->MatrixValues[Row*(this->Num_Cols) + Col] = Val;
}

template <class T>
Matrix<T> Matrix<T>::Transpose()
{
	T temp = 0;
	for(size_t R = 0; R < (Num_Rows-1); R++)
	{
		for(size_t C = 0; C < (Num_Cols-1); C++)
		{
			temp = MatrixValues[R*Num_Cols + (C+1)];
			MatrixValues[R*Num_Cols + (C+1)] = MatrixValues[(R+1)*Num_Cols + C];
			MatrixValues[(R+1)*Num_Cols + C] = temp;
		}
	}
	return Matrix(Num_Cols, Num_Rows, MatrixValues);
}

template <class T>
static Matrix<T> Matrix<T>::Linspace(const T& Start, const T& End, const size_t& NumPoints)
{
	size_t Num_Points = NumPoints;
	T* TempVec = new T[Num_Points];
	size_t count = 0;
	for(size_t i = 0; i <= NumPoints; i++)
	{
		TempVec[count++] = (T)((double)Start + i*(double)(End - Start)/((double)NumPoints - 1));
	}
	return Matrix(1, Num_Points, TempVec);
}

template <class T>
static Matrix<T> Matrix<T>::Range(const int& Start, const int& End, const size_t& Step)
{
	size_t NumElements = (End - Start)/Step+1;
	T* TempVec = new T[NumElements];
	size_t count = 0;
	for(size_t i = Start; i <= End; i += Step)
	{
		TempVec[count++] = i;
	}
	return Matrix(1, NumElements, TempVec);
}

#endif